import pytest
import gc
import os
from time import time
import logging
import shutil
import typing
from rdflib import Graph, Namespace, RDFS, RDF, URIRef
from rdflib.store import VALID_STORE
from test.data import TEST_DATA_DIR, context0, storename

logging.basicConfig(level=logging.ERROR, format="%(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


ukparl = TEST_DATA_DIR / "ukparl"
ukppdbpath = TEST_DATA_DIR / "ukparl" / "ukparl-working"
UKPARL = Namespace("http://bel-epa.com/ont/2007/6/ukpp.owl#")
DFOAF = Namespace("http://daml.umbc.edu/ontologies/cobra/0.4/foaf-basic#")
DGOV = Namespace("http://reliant.teknowledge.com/DAML/Government.owl#")

path = str(ukppdbpath)


@pytest.fixture
def graph() -> typing.Generator[Graph, None, None]:
    if os.path.exists(path):
        try:
            shutil.rmtree(path)
        except Exception:
            os.unlink(path)

    graph = Graph(store=storename)
    rt = graph.open(path, create=True)
    assert rt == VALID_STORE, "The underlying store is corrupt"
    assert (
        len(graph) == 0
    ), "There must be zero triples in the graph just after store (file) creation"

    yield graph

    graph.close()
    graph.destroy(configuration=path)


@pytest.fixture
def persisted() -> typing.Generator[Graph, None, None]:
    graph = Graph(store=storename)

    if os.path.exists(path):
        t0 = time()
        rt = graph.open(path, create=False)
        t1 = time()
        assert rt == VALID_STORE, "The underlying store is corrupt"
        assert (
            len(graph) == 113545
        ), "There must be 113545 triples in the graph just after store re-opening"
        logger.debug(
            f"Read from persistence: {t1 - t0:.3f}s"
        )  # Read from persistence: 0.017s

    else:
        rt = graph.open(path, create=True)
        assert rt == VALID_STORE, "The underlying store is corrupt"
        assert (
            len(graph) == 0
        ), "There must be zero triples in the graph just after store (file) creation"
        t0 = time()
        graph.parse(location=str(ukparl) + '/' + 'ukparl-tbox.xml', format="xml")
        graph.parse(location=str(ukparl) + '/' + 'ukparl-abox.xml', format="xml")
        t1 = time()
        assert len(graph) == 113545, len(graph)
        logger.debug(f"Parse into to store-backed graph: {t1 - t0:.3f}s")  # Parse into to store-backed graph: 17.815s

    yield graph


def test_create_and_populate_store_from_in_memory_graph(graph: Graph) -> None:
    memgraph = Graph("Memory", context0)
    gcold = gc.isenabled()
    gc.collect()
    gc.disable()

    t0 = time()
    memgraph.parse(location=str(ukparl) + '/' + 'ukparl-tbox.xml', format="xml")
    memgraph.parse(location=str(ukparl) + '/' + 'ukparl-abox.xml', format="xml")
    t1 = time()
    logger.debug(f"Parse time: {t1 - t0:.3f}s")  # Parse time: 10.284s

    t0 = time()
    for triple in memgraph.triples((None, None, None)):
        graph.add(triple)
    t1 = time()
    assert len(graph) == 113545, len(graph)
    logger.debug(f"Number of triples loaded {len(graph)}")
    memgraph.close()
    graph.close()
    logger.debug(f"Add to graph: {t1 - t0:.3f}s")  # Add to graph: 9.481s
    if gcold:
        gc.enable()


def test_create_and_populate_database_from_parse(graph: Graph) -> None:
    graph = graph

    gcold = gc.isenabled()
    gc.collect()
    gc.disable()

    t0 = time()
    graph.parse(location=str(ukparl) + '/' + 'ukparl-tbox.xml', format="xml")
    graph.parse(location=str(ukparl) + '/' + 'ukparl-abox.xml', format="xml")
    t1 = time()
    assert len(graph) == 113545, len(graph)
    graph.close()
    logger.debug(f"Load into to graph: {t1 - t0:.3f}s")  # Load into to graph: 17.815s
    if gcold:
        gc.enable()


def test_sparql_query(persisted) -> None:
    graph = persisted

    gcold = gc.isenabled()
    gc.collect()
    gc.disable()

    classquery = """prefix owl: <http://www.w3.org/2002/07/owl#>
prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>

SELECT DISTINCT ?class ?label ?description
WHERE {
  ?class a owl:Class.
  OPTIONAL { ?class rdfs:label ?label}
  OPTIONAL { ?class rdfs:comment ?description}
}
"""
    t0 = time()
    r = graph.query(classquery)
    t1 = time()
    logger.debug(f"Run SPARQL ?class ?label ?description query returning {len(r)} triples: {t1 - t0:.5f}s")

    triplesquery = """prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
prefix owl: <http://www.w3.org/2002/07/owl#>

SELECT ?subject ?predicate ?object
WHERE {
  ?subject ?predicate ?object
}
"""
    t0 = time()
    r = graph.query(triplesquery)
    t1 = time()
    logger.debug(f"Run SPARQL ?subject ?predicate ?object triples query returning {len(r)} triples: {t1 - t0:.5f}s")
    if gcold:
        gc.enable()
    graph.close()


def test_programmatic_query(persisted: Graph) -> None:
    graph = persisted

    graph.bind("ukparl", UKPARL)

    things: dict[str, int] = {}

    t0 = time()
    pquery = list(graph.triples((None, RDF.type, None)))
    t1 = time()
    logger.debug(f"Run triples query #1: {t1 - t0:.5f}s")

    t0 = time()
    for subj, pred, obj in pquery:
        if "ukpp" in str(obj):
            thing = obj.n3().split("#")[-1][:-1]  # type: ignore[attr-defined]
            if obj in things:  # type: ignore[comparison-overlap]
                things[thing] += 1
            else:
                things[thing] = 1

    assert sorted(list(things.keys())) == [
        'Area', 'Constituency', 'Department', 'HouseOfCommons', 'HouseOfLords',
        'LordOfParliament', 'LordOfParliamentRole', 'MemberOfParliament',
        'MemberOfParliamentRole', 'ParliamentaryRole', 'PartyAffiliation',
        'Region', 'UKGBNIParliament', 'UKParliament', 'UKPoliticalParty'
    ]
    t1 = time()
    logger.debug(f"Assertion check of triples query #1: {t1 - t0:.5f}s")

    t0 = time()
    ukpp_predicates = list(graph.predicates()) 
    t1 = time()
    logger.debug(f"Run predicates query: {t1 - t0:.5f}s")

    things = {}
    t0 = time()
    for pred in ukpp_predicates:
        if "ukpp" in str(pred):
            thing = pred.n3().split("#")[-1][:-1]  # type: ignore[attr-defined]
            if pred in things:  # type: ignore[comparison-overlap]
                things[thing] += 1
            else:
                things[thing] = 1

    assert sorted(list(things.keys())) == [
        'abolitionDate', 'abolitionFromDate', 'abolitionToDate', 'area', 'assembled',
        'context', 'country', 'countyName', 'dbpediaEntry', 'dissolved', 'duration',
        'elected', 'end', 'endingDate', 'establishedDate', 'familyName', 'foreNames',
        'foreNamesInFull', 'fromDate', 'fromWhy', 'givenName', 'hasConstituency',
        'hasMemberOfParliament', 'lordName', 'lordOfName', 'lordOfNameInFull',
        'majorityInSeat', 'name', 'note', 'number', 'parliament_number',
        'parliamentaryRole', 'party', 'partyAffiliation', 'peerageType',
        'prime_minister', 'region', 'reign', 'roleTaken', 'sessions', 'speaker',
        'start', 'startingDate', 'summoned', 'swingToLoseSeat', 'toDate', 'toWhy',
        'wikipediaEntry'
    ]
    t1 = time()
    logger.debug(f"Assertion check of predicates query #1: {t1 - t0:.5f}s")

    t0 = time()
    famnames = set(graph.subject_objects(predicate=UKPARL.familyName))
    t1 = time()
    logger.debug(f"Run subject_objects query: {t1 - t0:.5f}s")

    t0 = time()
    assert [(s.n3(), str(o)) for s, o in sorted(list(famnames))][:12] == [
        ('<http://bel-epa.com/ont/2007/6/ukpp.owl#ukpp-member-1>', 'Abbott'),
        ('<http://bel-epa.com/ont/2007/6/ukpp.owl#ukpp-member-10>', 'Ancram'),
        ('<http://bel-epa.com/ont/2007/6/ukpp.owl#ukpp-member-100>', 'Chaytor'),
        ('<http://bel-epa.com/ont/2007/6/ukpp.owl#ukpp-member-1000>', 'Johnson'),
        ('<http://bel-epa.com/ont/2007/6/ukpp.owl#ukpp-member-1001>', 'Jones'),
        ('<http://bel-epa.com/ont/2007/6/ukpp.owl#ukpp-member-1002>', 'Jones'),
        ('<http://bel-epa.com/ont/2007/6/ukpp.owl#ukpp-member-1003>', 'Jones'),
        ('<http://bel-epa.com/ont/2007/6/ukpp.owl#ukpp-member-1004>', 'Jones'),
        ('<http://bel-epa.com/ont/2007/6/ukpp.owl#ukpp-member-1005>', 'Jones'),
        ('<http://bel-epa.com/ont/2007/6/ukpp.owl#ukpp-member-1006>', 'Jones'),
        ('<http://bel-epa.com/ont/2007/6/ukpp.owl#ukpp-member-1007>', 'Jowell'),
        ('<http://bel-epa.com/ont/2007/6/ukpp.owl#ukpp-member-1008>', 'Joyce')
    ]
    t1 = time()
    logger.debug(f"Assertion check of subject_objects query #1: {t1 - t0:.5f}s")

    t0 = time()
    subclassesof = list(graph.triples((None, RDFS.subClassOf, DGOV.PoliticalParty)))
    t1 = time()
    logger.debug(f"Run triples query #2: {t1 - t0:.5f}s")

    s, p, o = subclassesof[0]
    assert s == UKPARL.UKPoliticalParty

    parties = [
        "Labour Party",
        "Conservative Party",
        "Green Party",
        "Co-operative Party",
        "Liberal Democrats",
        "Democratic Unionist Party",
        "UK Independence Party",
        "Plaid Cymru",
        "Social Democratic and Labour Party",
        "Scottish National Party",
        "Respect",
        "Sinn Féin",
        "Alliance Party of Northern Ireland",
        "Progressive Unionist Party",
        "Sinn Féin",
        "Ulster Democratic Unionist Party",
        "Ulster Unionist Party",
        "UK Unionist Party",
        "Ulster Unionist Party",
        "United Unionist Assembly Party",
        "UK Unionist Party",
        "Northern Ireland Unionist Party",
        "Northern Ireland Women’s Coalition",
        "Liberal Democrat",
        "Independent Unionist",
        "Independent",
        "Independent",
        "Independent Labour",
        "Independent Ulster Unionist",
        "Independent Conservative",
    ]

    t0 = time()
    ppps = list(graph.triples((None, RDF.type, UKPARL.UKPoliticalParty)))
    t1 = time()
    logger.debug(f"Run triples query #3: {t1 - t0:.5f}s")

    t0 = time()
    for pp, (s, p, o) in zip(parties, ppps):
        assert str(graph.value(s, RDFS.comment)) == pp
    t1 = time()
    logger.debug(f"Assertion check of triples query #3: {t1 - t0:.5f}s")

    t0 = time()
    diane_abbott = list(graph.triples(
        (
            URIRef("http://bel-epa.com/ont/2007/6/ukpp.owl#ukpp-member-1"),
            RDF.type,
            UKPARL.MemberOfParliament,
        ))
    )
    t1 = time()
    logger.debug(f"Run triples query #4: {t1 - t0:.5f}s")
    assert len(diane_abbott) == 1

    t0 = time()
    diane_abbott_names = list(graph.triples(
        (
            URIRef("http://bel-epa.com/ont/2007/6/ukpp.owl#ukpp-member-1"),
            DFOAF.name,
            None,
        ))
    )
    t1 = time()
    logger.debug(f"Run triples query #5: {t1 - t0:.5f}s")

    assert len(diane_abbott_names) == 1
    s, p, o = diane_abbott_names[0]
    assert str(o) == "Diane Abbott"

    graph.close()
