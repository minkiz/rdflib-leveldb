# -*- coding: utf-8 -*-
import pytest
import os
import re
import shutil
import tempfile
import typing
import logging
import rdflib
from time import time
from rdflib import ConjunctiveGraph, Dataset
from .data import bob, michel, tarek, hates, likes, cheese, pizza, context0, context1, context2, EX, storename

logging.basicConfig(level=logging.ERROR, format="%(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

path = os.path.join(tempfile.gettempdir(), f"test_{storename.lower()}.db")


def normalize_contexts(graph: typing.Union[Dataset, ConjunctiveGraph]) -> typing.List[rdflib.term.Node]:
    return [x.identifier if hasattr(x, "identifier") else x for x in graph.store.contexts()]


graphtypes = [
    ConjunctiveGraph,
    Dataset
]


@pytest.fixture(scope="function", params=graphtypes)
def graph(request: pytest.FixtureRequest) -> typing.Generator[typing.Union[Dataset, ConjunctiveGraph], None, None]:

    if os.path.exists(path):
        shutil.rmtree(path)

    _g = request.param(store=storename)
    _g.open(path, create=True)

    yield _g

    _g.close()
    _g.destroy(path)

    if os.path.exists(path):
        shutil.rmtree(path)


def test_triples_sans_context(graph: typing.Union[ConjunctiveGraph, Dataset]) -> None:

    graph.add((michel, likes, pizza))
    graph.add((michel, likes, cheese))
    graph.commit()
    ntriples = list(
        graph.triples((None, None, None), context=next(graph.contexts()))
    )
    assert len(ntriples) == 2, f"There should be 2 triples, not {len(ntriples)}"


def test_triples_in_context(graph: typing.Union[ConjunctiveGraph, Dataset]) -> None:

    graph.default_union = False
    # Note on type error: arg-type - Argument 1 to "add" of "ConjunctiveGraph"
    # has incompatible type "tuple[URIRef, URIRef, URIRef, URIRef]"; 
    # expected "tuple[Node, Node, Node] | tuple[Node, Node, Node, Graph | None]"
    graph.add((michel, likes, pizza, context1))
    # Note on type error: arg-type - Argument 1 to "add" of "ConjunctiveGraph"
    # has incompatible type "tuple[URIRef, URIRef, URIRef, URIRef]"; 
    # expected "tuple[Node, Node, Node] | tuple[Node, Node, Node, Graph | None]"
    graph.add((michel, likes, cheese, context1))
    graph.commit()
    # In context
    # Note on type error mypy: call-overload -
    # No overload variant of "triples" of "ConjunctiveGraph" matches
    # argument types "tuple[None, None, None]", "URIRef"
    # Possible overload variants:
    # def triples(self, triple_or_quad: tuple[Node | None, Node | None, Node | None] | tuple[Node | None, Node | None, Node | None, Graph | None], context: Graph | None = ...) -> Generator[tuple[Node, Node, Node], None, None]
    # def triples(self, triple_or_quad: tuple[Node | None, Path, Node | None] | tuple[Node | None, Path, Node | None, Graph | None], context: Graph | None = ...) -> Generator[tuple[Node, Path, Node], None, None]
    # def triples(self, triple_or_quad: tuple[Node | None, Path | Node | None, Node | None] | tuple[Node | None, Path | Node | None, Node | None, Graph | None], context: Graph | None = ...) -> Generator[tuple[Node, Node, Node] | tuple[Node, Path, Node], None, None]
    ntriples = list(
        graph.triples((None, None, None), context=context1)
    )
    assert len(ntriples) == 2, f"There should be 2 triples, not {len(ntriples)}"

    # In default (with default_union == False)
    ntriples = list(graph.triples((None, None, None)))
    assert len(ntriples) == 0, f"There should be 0 triples, not {len(ntriples)}"


def test_remove_context(graph: typing.Union[ConjunctiveGraph, Dataset]) -> None:

    graph.add((michel, likes, pizza))
    graph.add((michel, likes, cheese))
    graph.commit()
    graph.remove((michel, likes, cheese, next(graph.contexts())))
    graph.commit()
    ntriples = list(graph.triples((None, None, None)))
    assert len(ntriples) == 1, f"There should be 1 triple, not {len(ntriples)}"


def test_nquads_default(graph: typing.Union[ConjunctiveGraph, Dataset]) -> None:
    data = """
    <http://example.org/s1> <http://example.org/p1> <http://example.org/o1> .
    <http://example.org/s2> <http://example.org/p2> <http://example.org/o2> .
    <http://example.org/s3> <http://example.org/p3> <http://example.org/o3> <http://example.org/g3> .
    """

    public_id = rdflib.URIRef("http://example.org/g0")

    graph.parse(data=data, format="nquads", publicID=public_id)

    assert len(graph) == 3, f"len Graph should be 3, not {len(graph)}"

    # Dependent on philosophy of whether Dataset should include default in contexts
    expected = 2 if isinstance(graph, ConjunctiveGraph) else 3
    actual = len(list(graph.store.contexts()))

    assert actual == expected, f"Number of contexts should be {expected}, not {actual}"

    assert len(graph.default_context) == 2, len(
        graph.get_context(public_id)
    )


def test_serialize(graph: typing.Union[ConjunctiveGraph, Dataset]) -> None:

    graph.get_context(context0).add((bob, likes, pizza))
    graph.get_context(context1).add((bob, likes, pizza))

    serialized_graph = graph.serialize(format="nquads")
    assert len([x for x in serialized_graph.split("\n") if x.strip()]) == 2

    g2 = Dataset(store="LevelDB") if isinstance(graph, Dataset) else ConjunctiveGraph(store="LevelDB")

    g2.open(tempfile.mktemp(prefix="leveldbstoretest"), create=True)
    g2.parse(data=serialized_graph, format="nquads")

    assert len(graph) == len(g2)

    graph_contexts = normalize_contexts(graph)
    g2_contexts = normalize_contexts(g2)

    assert sorted(graph_contexts, key=lambda x: str(x)) == sorted(g2_contexts, key=lambda x: str(x))


def test_store_graph_aware(graph: typing.Union[ConjunctiveGraph, Dataset]) -> None:

    if not graph.store.graph_aware:
        return

    # Note on type error: arg-type - Argument 1 to "add_graph" of "Store"
    # has incompatible type "URIRef"; expected "Graph" --- of *course* it does.
    graph.store.add_graph(context1)

    # added graph exists
    assert set(normalize_contexts(graph)) == set([context1])


def test_add_graph(graph: typing.Union[ConjunctiveGraph, Dataset]) -> None:

    if not graph.store.graph_aware:
        return

    g1 = graph.graph(context1) if isinstance(graph, Dataset) else graph.get_context(context1)

    # added graph exists
    assert set(normalize_contexts(graph)) == set([context1] if isinstance(graph, Dataset) else [])
    # added graph is empty
    assert len(g1) == 0

    g1.add((tarek, likes, pizza))

    # added graph still exists
    actual = set(normalize_contexts(graph))
    assert actual == set([context1])

    # added graph contains one triple
    assert len(g1) == 1

    g1.remove((tarek, likes, pizza))

    # added graph is empty
    assert len(g1) == 0

    # graph still exists, although empty
    actual = set(normalize_contexts(graph))
    assert actual == set([context1])

    assert isinstance(context1, rdflib.term.IdentifiedNode)

    # Note on type error: arg-type - Argument 1 to "add_graph" of "Store"
    # has incompatible type "URIRef"; expected "Graph" --- of *course* it does.
    graph.store.remove_graph(context1)

    # context is preserved
    actual = set(normalize_contexts(graph))
    assert actual == set([context1])


def test_default_graph(graph: typing.Union[ConjunctiveGraph, Dataset]) -> None:
    # Something the default graph is read-only (e.g. TDB in union mode)

    graph.add((tarek, likes, pizza))
    assert len(graph) == 1
    # only default exists
    assert list(graph.contexts()) == [graph.default_context]

    # removing default graph removes triples but not actual graph
    # Note on type error: arg-type - Argument 1 to "add_graph" of "Store"
    # has incompatible type "URIRef"; expected "Graph" --- of *course* it does.
    graph.store.remove_graph(rdflib.graph.DATASET_DEFAULT_GRAPH_ID)

    assert len(graph) == 1

    # default still exists
    assert set(graph.contexts()) == set([graph.default_context])


def test_not_union(graph: typing.Union[ConjunctiveGraph, Dataset]) -> None:

    # Union depends on the SPARQL endpoint configuration
    graph.default_union = False
    subgraph1 = graph.graph(context1) if isinstance(graph, Dataset) else graph.get_context(context1)
    subgraph1.add((tarek, likes, pizza))

    assert list(graph.objects(tarek, None)) == []
    assert list(subgraph1.objects(tarek, None)) == [pizza]


def test_iter(graph: typing.Union[ConjunctiveGraph, Dataset]) -> None:

    """PR 1382: adds __iter__ to Dataset"""
    uri_a = rdflib.URIRef("https://example.com/a")
    uri_b = rdflib.URIRef("https://example.com/b")
    uri_c = rdflib.URIRef("https://example.com/c")
    uri_d = rdflib.URIRef("https://example.com/d")

    graph.graph(context1) if isinstance(graph, Dataset) else graph.get_context(context1)

    graph.add((uri_a, uri_b, uri_c, context1))  # type: ignore[arg-type]

    graph.add(
        (uri_a, uri_b, uri_c, context1)  # type: ignore[arg-type]
    )  # pointless addition: duplicates above

    graph.graph(context2) if isinstance(graph, Dataset) else graph.get_context(context2)
    graph.add((uri_a, uri_b, uri_c, context2))  # type: ignore[arg-type]
    graph.add((uri_a, uri_b, uri_d, context1))  # type: ignore[arg-type]

    # traditional iterator
    i_trad = 0
    for t in graph.quads((None, None, None)):
        i_trad += 1

    # new Dataset.__iter__ iterator
    i_new = 0
    # Note on type error: assignment - Incompatible types in assignment
    # (expression has type "tuple[Node, Node, Node] | tuple[Node, Node, Node, IdentifiedNode | None]",
    # variable has type "tuple[Node, Node, Node, Graph | None] | tuple[Node, Node, Node, IdentifiedNode | None]")
    for t in graph:
        i_new += 1
    if isinstance(graph, Dataset):
        assert i_new == i_trad  # both should be 3
    else:
        assert i_new + 1 == i_trad  # both should be 3


def test_subgraph_without_identifier(graph: typing.Union[ConjunctiveGraph, Dataset]) -> None:
    """
    Dataset subgraphs with no identifiers assigned are identified by Skolem IRIs with a
    prefix that is bound to `genid`.

    TODO: This is somewhat questionable and arbitrary behaviour and should be
    reviewed at some point.
    """

    nman = graph.namespace_manager

    genid_prefix = rdflib.URIRef("https://rdflib.github.io/.well-known/genid/rdflib/")

    namespaces = set(nman.namespaces())
    assert (
        next((namespace for namespace in namespaces if namespace[0] == "genid"), None)
        is None
    )

    # Behaviour only defined for Dataset, so fake it for ConjunctiveGraph
    subgraph: rdflib.Graph = graph.graph() if isinstance(graph, Dataset) else graph.get_context(identifier=genid_prefix)
    subgraph.add((EX["subject"], EX["predicate"], EX["object"]))

    namespaces = set(nman.namespaces())

    # Behaviour only defined for Dataset
    if isinstance(graph, Dataset):
        assert next(
            (namespace for namespace in namespaces if namespace[0] == "genid"), None
        ) == ("genid", genid_prefix)

    assert f"{subgraph.identifier}".startswith(genid_prefix)


def test_write_parsed(graph: typing.Union[ConjunctiveGraph, Dataset]) -> None:

    data = """
            PREFIX : <https://example.org/>

            :a :b :c .
            :d :e :f .
            :d :g :h .
            """
    graph.parse(data=data, format="ttl")
    assert (
        len(graph) == 3
    ), "There must be three triples in the graph after the first data chunk parse"

    data2 = """
            PREFIX : <https://example.org/>

            :d :i :j .
            """
    graph.parse(data=data2, format="ttl")
    assert (
        len(graph) == 4
    ), "There must be four triples in the graph after the second data chunk parse"

    data3 = """
            PREFIX : <https://example.org/>

            :d :i :j .
            """
    graph.parse(data=data3, format="ttl")
    assert (
        len(graph) == 4
    ), "There must still be four triples in the graph after the thrd data chunk parse"


def test_read(graph: typing.Union[ConjunctiveGraph, Dataset]) -> None:

    data = """
            PREFIX : <https://example.org/>

            :a :b :c .
            :d :e :f .
            :d :g :h .
            """
    graph.parse(data=data, format="ttl")
    assert (
        len(graph) == 3
    ), "There must be three triples in the graph after the first data chunk parse"

    sx = None
    for s in graph.subjects(
        predicate=rdflib.URIRef("https://example.org/e"),
        object=rdflib.URIRef("https://example.org/f"),
    ):
        sx = s
    assert sx ==rdflib.URIRef("https://example.org/d")


def test_sparql_query(graph: typing.Union[ConjunctiveGraph, Dataset]) -> None:

    data = """
            PREFIX : <https://example.org/>

            :a :b :c .
            :d :e :f .
            :d :g :h .
            """
    graph.parse(data=data, format="ttl")
    assert (
        len(graph) == 3
    ), "There must be three triples in the graph after the first data chunk parse"

    q = r"""
        PREFIX : <https://example.org/>

        SELECT (COUNT(*) AS ?c)
        WHERE {
            :d ?p ?o .
        }"""

    c = 0
    for row in graph.query(q):
        c = int(row.c)  # type: ignore[union-attr]
    assert c == 2, "SPARQL COUNT must return 2"


def test_sparql_insert(graph: typing.Union[ConjunctiveGraph, Dataset]) -> None:

    data = """
            PREFIX : <https://example.org/>

            :a :b :c .
            :d :e :f .
            :d :g :h .
            """
    graph.parse(data=data, format="ttl")
    assert (
        len(graph) == 3
    ), "There must be three triples in the graph after the first data chunk parse"

    q = r"""
        PREFIX : <https://example.org/>

        INSERT DATA {
            :x :y :z .
        }"""

    graph.update(q)
    assert len(graph) == 4, "After extra triple insert, length must be 4"


def test_multigraph(graph: typing.Union[ConjunctiveGraph, Dataset]) -> None:

    q = r"""
        PREFIX : <https://example.org/>

        INSERT DATA {
            GRAPH :m {
                :x :y :z .
            }
            GRAPH :n {
                :x :y :z .
            }
        }"""

    graph.update(q)

    q = """
        SELECT (COUNT(?g) AS ?c)
        WHERE {
            SELECT DISTINCT ?g
            WHERE {
                GRAPH ?g {
                    ?s ?p ?o
                }
            }
        }
        """
    c = 0
    for row in graph.query(q):
        c = int(row.c)  # type: ignore[union-attr]
    assert c == 2, "SPARQL COUNT must return 2 (default, :m & :n)"


def test_open_shut(graph: typing.Union[ConjunctiveGraph, Dataset]) -> None:

    data = """
            PREFIX : <https://example.org/>

            :a :b :c .
            :d :e :f .
            :d :g :h .
            """
    graph.parse(data=data, format="ttl")
    assert (
        len(graph) == 3
    ), "There must be three triples in the graph after the first data chunk parse"

    assert len(graph) == 3, "Initially we must have 3 triples from setUp"
    graph.close()

    # reopen the graph
    graph = Dataset("LevelDB") if isinstance(graph, Dataset) else ConjunctiveGraph("LevelDB")
    graph.open(path, create=False)
    assert (
        len(graph) == 3
    ), f"After close and reopen, we should still have the 3 originally added triples, not {len(graph)}"
    graph.close()
    graph.destroy(path)


implies = rdflib.URIRef("http://www.w3.org/2000/10/swap/log#implies")
testN3 = """
@prefix rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix : <http://test/> .
{:a :b :c;a :foo} => {:a :d :c,?y}.
_:foo a rdfs:Class.
:a :d :c."""  # noqa: N816

# Thorough test suite for formula-aware store


def test_formula_store(graph: typing.Union[ConjunctiveGraph, Dataset]) -> None:

    graph.parse(data=testN3, format="n3")
    for s, p, o in graph.triples((None, implies, None)):
        formula_a = s  # noqa: N806
        formula_b = o  # noqa: N806

    assert type(formula_a) == rdflib.graph.QuotedGraph and type(formula_b) == rdflib.graph.QuotedGraph
    a = rdflib.URIRef('http://test/a')
    b = rdflib.URIRef("http://test/b")
    c = rdflib.URIRef("http://test/c")
    d = rdflib.URIRef("http://test/d")
    v = rdflib.Variable("y")

    universe = Dataset(graph.store) if isinstance(graph, Dataset) else ConjunctiveGraph(graph.store)

    # test formula as terms
    assert len(list(universe.triples((formula_a, implies, formula_b)))) == 1

    # test variable as term and variable roundtrip
    assert len(list(formula_b.triples((None, None, v)))) == 1
    for s, p, o in formula_b.triples((None, d, None)):
        if o != c:
            assert isinstance(o, rdflib.Variable)
            assert o == v
    s = list(universe.subjects(rdflib.RDF.type, rdflib.RDFS.Class))[0]
    assert isinstance(s, rdflib.BNode)
    assert len(list(universe.triples((None, implies, None)))) == 1
    assert len(list(universe.triples((None, rdflib.RDF.type, None)))) == 1
    assert len(list(formula_a.triples((None, rdflib.RDF.type, None)))) == 1
    assert len(list(formula_a.triples((None, None, None)))) == 2
    assert len(list(formula_b.triples((None, None, None)))) == 2
    assert len(list(universe.triples((None, None, None)))) == 3
    assert len(list(formula_b.triples((None, rdflib.URIRef("http://test/d"), None)))) == 2
    assert len(list(universe.triples((None, rdflib.URIRef("http://test/d"), None)))) == 1

    # context tests
    # test contexts with triple argument
    assert len(list(universe.contexts((a, d, c)))) == 1, [ct for ct in universe.contexts((a, d, c))]

    # Remove test cases
    # arg-type - Argument 1 to "remove" of "ConjunctiveGraph" has
    # incompatible type "tuple[None, URIRef, None]";
    # expected          "tuple[Node, Node, Node] | 
    #                    tuple[Node, Node, Node, Graph | None]"
    universe.remove((None, implies, None))  # type: ignore[arg-type]
    assert len(list(universe.triples((None, implies, None)))) == 0
    assert len(list(formula_a.triples((None, None, None)))) == 2
    assert len(list(formula_b.triples((None, None, None)))) == 2

    formula_a.remove((None, b, None))
    assert len(list(formula_a.triples((None, None, None)))) == 1
    formula_a.remove((None, rdflib.RDF.type, None))
    assert len(list(formula_a.triples((None, None, None)))) == 0

    universe.remove((None, rdflib.RDF.type, rdflib.RDFS.Class))  # type: ignore[arg-type]

    # remove_context tests
    universe.remove_context(formula_b)
    assert len(list(universe.triples((None, rdflib.RDF.type, None)))) == 0
    assert len(universe) == 1
    assert len(formula_b) == 0

    universe.remove((None, None, None))  # type: ignore[arg-type]
    assert len(universe) == 0


def test_simplegraph(graph: typing.Union[ConjunctiveGraph, Dataset]) -> None:

    t0 = time()
    g = graph.get_context(context0)
    g.add((tarek, likes, pizza))
    g.add((bob, likes, pizza))
    g.add((bob, likes, cheese))

    g2 = graph.get_context(context1)
    g2.add((michel, likes, pizza))

    assert 3 == len(g), "graph contains 3 triples"
    assert 1 == len(g2), "other graph contains 1 triple"

    r = g.query("SELECT * WHERE { ?s <urn:example:likes> <urn:example:pizza> . }")
    assert 2 == len(list(r)), "two people like pizza"

    n = g.triples((None, likes, pizza))
    assert 2 == len(list(n)), "two people like pizza"

    # Test initBindings
    r = g.query(
        "SELECT * WHERE { ?s <urn:example:likes> <urn:example:pizza> . }",
        initBindings={"s": tarek},
    )
    assert 1 == len(list(r)), "i was asking only about tarek"

    n = g.triples((tarek, likes, pizza))
    assert 1 == len(list(n)), "i was asking only about tarek"

    n = g.triples((tarek, likes, cheese))
    assert 0 == len(list(n)), "tarek doesn't like cheese"

    g2.add((tarek, likes, pizza))
    g.remove((tarek, likes, pizza))
    r = g.query("SELECT * WHERE { ?s <urn:example:likes> <urn:example:pizza> . }")
    t1 = time()

    # logger.debug(f"test_simplegraph {graph.__class__.__name__}: {t1 - t0:.5f}")


def test_namedgraph_default(graph: typing.Union[ConjunctiveGraph, Dataset]) -> None:

    graph.default_union = True
    t0 = time()

    g1 = graph.get_context(context0)
    g1.add((tarek, likes, pizza))

    g2 = graph.get_context(context1)
    g2.add((bob, likes, pizza))

    g1.add((tarek, hates, cheese))

    assert 2 == len(g1), "graph contains 2 triples"

    assert 3 == len(graph), f"default union graph should contain three triples but contains {len(graph)}"

    r = graph.query("SELECT * WHERE { ?s <urn:example:likes> <urn:example:pizza> . }")
    assert 2 == len(list(r)), "two people like pizza"

    r = graph.query(
        "SELECT * WHERE { ?s <urn:example:likes> <urn:example:pizza> . }",
        initBindings={"s": tarek},
    )
    assert 1 == len(list(r)), "i was asking only about tarek"

    n = graph.triples((tarek, likes, pizza))
    assert 1 == len(list(n)), "i was asking only about tarek"

    n = graph.triples((tarek, likes, cheese))
    assert 0 == len(list(n)), "tarek doesn't like cheese"

    n = graph.triples((tarek, hates, cheese))
    assert 1 == len(list(n)), "tarek doesn't like cheese"

    g2.remove((bob, likes, pizza))

    r = graph.query("SELECT * WHERE { ?s <urn:example:likes> <urn:example:pizza> . }")
    assert 1 == len(list(r)), "only tarek likes pizza"
    t1 = time()
    logger.debug(f"test_conjunctivegraph_default {graph.__class__.__name__}: {t1 - t0:.5f}")


def test_update(graph: typing.Union[ConjunctiveGraph, Dataset]) -> None:

    t0 = time()
    graph.update(
        "INSERT DATA { GRAPH <urn:example:context-0> "
        "{ <urn:example:michel> <urn:example:likes> <urn:example:pizza> . } }"
    )
    g = graph.get_context(context0)
    assert 1 == len(g), "graph contains 1 triples"
    t1 = time()
    logger.debug(f"test_update {graph.__class__.__name__}: {t1 - t0:.5f} ")


def test_update_with_initns(graph: typing.Union[ConjunctiveGraph, Dataset]) -> None:

    t0 = time()
    graph.update(
        "INSERT DATA { GRAPH ns:context-0 { ns:michel ns:likes ns:pizza . } }",
        initNs={"ns": rdflib.URIRef("urn:example:")},
    )

    g = graph.get_context(context0)
    assert set(g.triples((None, None, None))) == set([(michel, likes, pizza)]), "only michel likes pizza"
    t1 = time()
    logger.debug(f"test_update_with_initns {graph.__class__.__name__}: {t1 - t0:.5f}")


def test_update_with_initbindings(graph: typing.Union[ConjunctiveGraph, Dataset]) -> None:

    t0 = time()
    graph.update(
        "INSERT { GRAPH <urn:example:context-0> { ?a ?b ?c . } } WherE { }",
        initBindings={
            "a": michel,
            "b": likes,
            "c": pizza,
        },
    )

    g = graph.get_context(context0)
    assert set(g.triples((None, None, None))) == set([(michel, likes, pizza)]), "only michel likes pizza"
    t1 = time()
    logger.debug(f"test_update_with_initbindings {graph.__class__.__name__}: {t1 - t0:.5f}")


def test_multiple_update_with_initbindings(graph: typing.Union[ConjunctiveGraph, Dataset]) -> None:

    t0 = time()
    graph.update(
        "INSERT { GRAPH <urn:example:context-0> { ?a ?b ?c . } } WHERE { };"
        "INSERT { GRAPH <urn:example:context-0> { ?d ?b ?c . } } WHERE { }",
        initBindings={
            "a": michel,
            "b": likes,
            "c": pizza,
            "d": bob,
        },
    )

    g = graph.get_context(context0)
    assert set(g.triples((None, None, None))) == set(
        [(michel, likes, pizza), (bob, likes, pizza)]
    ), "michel and bob like pizza"
    t1 = time()
    logger.debug(
        f"test_multiple_update_with_initbindings {graph.__class__.__name__}: {t1 - t0:.5f}"
    )


def test_named_graph_update(graph: typing.Union[ConjunctiveGraph, Dataset]) -> None:

    t0 = time()
    g = graph.get_context(context0)
    r1 = "INSERT DATA { <urn:example:michel> <urn:example:likes> <urn:example:pizza> }"
    g.update(r1)
    assert set(g.triples((None, None, None))) == set([(michel, likes, pizza)]), "only michel likes pizza"

    r2 = (
        "DELETE { <urn:example:michel> <urn:example:likes> <urn:example:pizza> } "
        + "INSERT { <urn:example:bob> <urn:example:likes> <urn:example:pizza> } WHERE {}"
    )
    g.update(r2)
    assert set(g.triples((None, None, None))) == set([(bob, likes, pizza)]), "only bob likes pizza"

    says = rdflib.URIRef("urn:example:says")

    # Strings with unbalanced curly braces
    tricky_strs = [
        "With an unbalanced curly brace %s " % brace
        for brace in ["{", "}"]
    ]
    for tricky_str in tricky_strs:
        r3 = (
            """INSERT { ?b <urn:example:says> "%s" }
        WHERE { ?b <urn:example:likes> <urn:example:pizza>} """
            % tricky_str
        )
        g.update(r3)

    values = set()
    for v in g.objects(bob, says):
        values.add(str(v))
    assert values == set(tricky_strs)

    # Complicated Strings
    r4strings = []
    r4strings.append(r'''"1: adfk { ' \\\" \" { "''')
    r4strings.append(r'''"2: adfk } <foo> #éï \\"''')

    r4strings.append(r"""'3: adfk { " \\\' \' { '""")
    r4strings.append(r"""'4: adfk } <foo> #éï \\'""")

    r4strings.append(r'''"""5: adfk { ' \\\" \" { """''')
    r4strings.append(r'''"""6: adfk } <foo> #éï \\"""''')
    r4strings.append('"""7: ad adsfj \n { \n sadfj"""')

    r4strings.append(r"""'''8: adfk { " \\\' \' { '''""")
    r4strings.append(r"""'''9: adfk } <foo> #éï \\'''""")
    r4strings.append("'''10: ad adsfj \n { \n sadfj'''")

    r4 = "\n".join(
        [
            "INSERT DATA { <urn:example:michel> <urn:example:says> %s } ;" % s
            for s in r4strings
        ]
    )
    g.update(r4)
    values = set()
    for v in g.objects(michel, says):
        values.add(str(v))
    assert values == set(
        [
            re.sub(
                r"\\(.)",
                r"\1",
                re.sub(
                    r"^'''|'''$|^'|'$|" + r'^"""|"""$|^"|"$', r"", s
                ),
            )
            for s in r4strings
        ]
    )

    # IRI Containing ' or #
    # The fragment identifier must not be misinterpreted as a comment
    # (commenting out the end of the block).
    # The ' must not be interpreted as the start of a string, causing the }
    # in the literal to be identified as the end of the block.
    r5 = """INSERT DATA { <urn:example:michel> <urn:example:hates> <urn:example:foo'bar?baz;a=1&b=2#fragment>, "'}" }"""

    g.update(r5)
    values = set()
    for v in g.objects(michel, hates):
        values.add(str(v))
    assert values == set(["urn:example:foo'bar?baz;a=1&b=2#fragment", "'}"])

    # Comments
    r6 = """
        INSERT DATA {
            <urn:example:bob> <urn:example:hates> <urn:example:bob> . # No closing brace: }
            <urn:example:bob> <urn:example:hates> <urn:example:michel>.
        }
    # Final { } comment"""

    g.update(r6)
    values = set()
    for v in g.objects(bob, hates):
        values.add(v)  # type: ignore[arg-type]
    assert values == set([bob, michel])
    t1 = time()
    logger.debug(f"test_named_graph_update {graph.__class__.__name__}: {t1 - t0:.5f}")


def test_named_graph_update_with_initbindings(graph: typing.Union[ConjunctiveGraph, Dataset]) -> None:

    t0 = time()
    g = graph.get_context(context0)
    r = "INSERT { ?a ?b ?c } WHERE {}"
    g.update(r, initBindings={"a": michel, "b": likes, "c": pizza})
    assert set(g.triples((None, None, None))) == set([(michel, likes, pizza)]), "only michel likes pizza"
    t1 = time()
    logger.debug(
        f"test_named_graph_update_with_initbindings {graph.__class__.__name__}: {t1 - t0:.5f}"
    )


def test_sqlitedb_named_graph_initial_contexts(graph: typing.Union[ConjunctiveGraph, Dataset]) -> None:

    if isinstance(graph, Dataset):
        assert [g.identifier for g in list(graph.contexts())] == [
            rdflib.graph.DATASET_DEFAULT_GRAPH_ID
        ]
        assert list(graph.store.contexts()) == [graph.default_context]
    else:
        assert [g.identifier for g in list(graph.contexts())] == []
        assert list(graph.store.contexts()) == []


def test_named_graph_retrieve_contexts(graph: typing.Union[ConjunctiveGraph, Dataset]) -> None:
    graph.add((tarek, likes, pizza))
    assert list(graph.contexts((tarek, likes, pizza))) == [graph.default_context]
