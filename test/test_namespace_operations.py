import pytest
import os
import shutil
import tempfile
import typing
import logging
import rdflib
from rdflib_leveldb.leveldb import LevelDB
from .data import context1, storename, EX, EG

logging.basicConfig(level=logging.ERROR, format="%(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

path = os.path.join(tempfile.gettempdir(), f"test_{storename.lower()}.db")


@pytest.fixture(scope="function")
def store() -> typing.Generator[rdflib.store.Store, None, None]:
    if os.path.exists(path):
        shutil.rmtree(path)

    store = LevelDB(path, identifier=context1)
    yield store
    store.close()
    store.destroy(path)

    if os.path.exists(path):
        shutil.rmtree(path)


def test_namespace(store: LevelDB) -> None:
    assert list(store.namespaces()) == []
    store.bind("ex", rdflib.URIRef(str(EX)))
    assert list(store.namespaces()) == [('ex', rdflib.term.URIRef('urn:example:'))]
    assert store.prefix(rdflib.URIRef(str(EX))) == 'ex'
    assert store.namespace('ex') == rdflib.term.URIRef('urn:example:')


def test_bind(store: LevelDB) -> None:
    nns = len(list(store.namespaces()))
    assert nns == 0

    store.bind("dc", rdflib.URIRef("http://http://purl.org/dc/elements/1.1/"))
    store.bind("foaf", rdflib.URIRef("http://xmlns.com/foaf/0.1/"))
    assert (
        len(list(store.namespaces())) == nns + 2
    )
    assert ("foaf", rdflib.URIRef("http://xmlns.com/foaf/0.1/")) in list(
        store.namespaces()
    )


def test_nsbind_unbind(store: LevelDB) -> None:

    store.bind("ex", rdflib.URIRef(str(EX)))
    store.bind("ez", rdflib.URIRef(str(EX)), override=False)
    store.bind("ex", rdflib.URIRef(str(EG)), override=False)
    store.bind("ex", rdflib.URIRef(str(EX)))
    store.unbind("ex")
    store.unbind("eu")


# def test_namespace_bind(graph: rdflib.Graph) -> None:
#     nns = len(list(graph.namespaces()))
#     graph.bind("dc", "http://http://purl.org/dc/elements/1.1/")
#     graph.bind("foaf", "http://xmlns.com/foaf/0.1/")
#     assert (
#         len(list(graph.namespaces())) == nns + 1
#     )
#     assert ("foaf", rdflib.URIRef("http://xmlns.com/foaf/0.1/")) in list(
#         graph.namespaces()
#     )


