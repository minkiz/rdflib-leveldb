import pytest
import os
import shutil
import tempfile
import typing
import logging
import rdflib
from rdflib_leveldb.leveldb import LevelDB, readable_index
from .data import tarek, michel, hates, likes, cheese, pizza, context0, context1, context2, storename, EX

logging.basicConfig(level=logging.ERROR, format="%(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

path = os.path.join(tempfile.gettempdir(), f"test_{storename.lower()}.db")


def test_readable_index() -> None:
    assert readable_index(111) == "s, p, o"
    assert readable_index(000) == "?, ?, ?"
    assert readable_index(111) == "s, p, o"
    assert readable_index(110) == "?, p, o"
    assert readable_index(333) == "s, ?, o"
    assert readable_index(211) == "s, p, ?"


@pytest.fixture(scope="function")
def store() -> typing.Generator[rdflib.store.Store, None, None]:
    if os.path.exists(path):
        shutil.rmtree(path)

    _s = LevelDB(path, identifier=context1)
    yield _s
    _s.close()
    _s.destroy(path)

    if os.path.exists(path):
        shutil.rmtree(path)


def test_coverage(store: LevelDB) -> None:
    assert store.is_open() is True
    assert len(store) == 0
    assert list(store.triples((None, None, None), store)) == []
    _g = rdflib.Graph(identifier=context0)
    store.add_graph(_g)
    store.add((tarek, likes, pizza), context=_g)
    store.remove((tarek, likes, pizza), context=_g)
    store.remove_graph(_g)
    _g = rdflib.Graph(identifier=context1)
    store.remove_graph(_g)
    store.remove_graph(context2)


def test_sqlitedb_dumpdb(store: LevelDB) -> None:

    store.bind("ex", rdflib.URIRef(str(EX)))
    store.add((tarek, likes, pizza), context=context2)

    assert store.dumpdb() == (
        "\n"
        "indices\n"
        "  cspo\n"
        "    “b'4^1^2^3^'” “b''”\n"
        "    “b'^1^2^3^'” “b'^4'”\n"
        "  cpos\n"
        "    “b'4^2^3^1^'” “b''”\n"
        "    “b'^2^3^1^'” “b'^4'”\n"
        "  cosp\n"
        "    “b'4^3^1^2^'” “b''”\n"
        "    “b'^3^1^2^'” “b'^4'”\n"
        "lookup\n"
        "  “0” = “[(b'4^1^2^3^', b''), (b'^1^2^3^', b'^4')]”\n"
        "  “1” = “[(b'4^1^2^3^', b''), (b'^1^2^3^', b'^4')]”\n"
        "  “2” = “[(b'4^2^3^1^', b''), (b'^2^3^1^', b'^4')]”\n"
        "  “3” = “[(b'4^1^2^3^', b''), (b'^1^2^3^', b'^4')]”\n"
        "  “4” = “[(b'4^3^1^2^', b''), (b'^3^1^2^', b'^4')]”\n"
        "  “5” = “[(b'4^3^1^2^', b''), (b'^3^1^2^', b'^4')]”\n"
        "  “6” = “[(b'4^2^3^1^', b''), (b'^2^3^1^', b'^4')]”\n"
        "  “7” = “[(b'4^1^2^3^', b''), (b'^1^2^3^', b'^4')]”\n"
        "namespaces:\n"
        "  “b'ex'” = “b'urn:example:'”\n"
        "contexts:\n"
        "  ctxt b'4' (“urn:example:context-2”) = “b''”\n"
        "indexed identifiers (k2i/i2k):\n"
        "  term b'urn:example:likes' = b'2'\n"
        "  term b'urn:example:pizza' = b'3'\n"
        "  term b'urn:example:tarek' = b'1'\n"
        "  ctxt b'urn:example:context-2' = b'4'\n"
        "  total: b'4'\n "
    )


def test_graph_store_shared_graph_as_context(store: LevelDB) -> None:

    graph = rdflib.Graph(store=store, identifier=context1)
    assert (
        len(graph) == 0
    ), "There must be zero triples in the graph just after store (file) creation"

    # NOTE: see below, different result if IRI passed as context
    store.add((tarek, likes, pizza), context=graph)

    assert (
        len(graph) == 1
    ), f"Expecting 1 triple in the graph, not {len(graph)} vs. {len(store)}"


def test_graph_store_shared_iri_as_context(store: LevelDB) -> None:

    graph = rdflib.Graph(store=store, identifier=context1)
    assert (
        len(graph) == 0
    ), "There must be zero triples in the graph just after store (file) creation"

    store.add((tarek, likes, pizza), context=context1)

    assert (
        len(graph) == 0
    ), f"Expecting 0 triples in the graph, not {len(graph)} vs. {len(store)}"


def test_add_triple(store: LevelDB) -> None:

    assert store.identifier == context1

    assert len(store) == 0

    assert list(store.triples((None, None, None))) == []
    store.add((tarek, likes, pizza), context2, quoted=False)
    assert list(store.triples((None, None, None)))[0][0] == (
        rdflib.term.URIRef('urn:example:tarek'),
        rdflib.term.URIRef('urn:example:likes'),
        rdflib.term.URIRef('urn:example:pizza')
    )


def test_graph_binding(store: LevelDB) -> None:

    graph = rdflib.Graph(store=store, identifier=context0)

    assert list(store.triples((None, None, None), graph)) == []
    assert list(store.triples((None, None, None), None)) == []

    store.add((tarek, likes, pizza), graph)

    # Note: Graph object as context
    triples = list(store.triples((None, None, None), graph))
    assert triples[0][0] == (
        rdflib.term.URIRef('urn:example:tarek'),
        rdflib.term.URIRef('urn:example:likes'),
        rdflib.term.URIRef('urn:example:pizza')
    )

    triples = list(store.triples((None, None, None), None))
    assert triples[0][0] == (
        rdflib.term.URIRef('urn:example:tarek'),
        rdflib.term.URIRef('urn:example:likes'),
        rdflib.term.URIRef('urn:example:pizza')
    )

    assert list(graph.triples((None, None, None))) == [(tarek, likes, pizza)]


def test_contexts(store: LevelDB) -> None:

    contextgraph0 = rdflib.Graph(identifier=context0)
    contextgraph1 = rdflib.Graph(identifier=context0)
    store.add((tarek, likes, pizza), contextgraph0)
    assert list(store.contexts((tarek, likes, pizza)))[0].identifier == context0
    assert list(store.contexts((None, likes, pizza)))[0].identifier == context0
    store.add((michel, likes, pizza), contextgraph1)
    assert len(list(store.contexts((None, likes, pizza)))) == 2
    store.remove((tarek, likes, pizza), store)
    store.remove((tarek, likes, pizza), contextgraph1)


def test_quotedgraph(store: LevelDB) -> None:
    from rdflib.plugins.parsers.notation3 import LOG_implies_URI
    LOG_implies = rdflib.URIRef(LOG_implies_URI)  # noqa: N806

    triple_a = (tarek, likes, pizza)
    triple_b = (tarek, likes, cheese)

    graph = rdflib.Graph(store=store, identifier=context0)
    graph.bind("", EX)

    qgraph_a = rdflib.graph.QuotedGraph(graph.store, context1)
    qgraph_a.add(triple_a)

    qgraph_b = rdflib.graph.QuotedGraph(graph.store, context2)
    graph.add((qgraph_a, LOG_implies, qgraph_b))
    qgraph_b.add(triple_b)

    assert graph.serialize(format="n3") == (
        "@prefix : <urn:example:> .\n"
        "\n"
        "{\n"
        "    :tarek :likes :pizza .\n"
        "\n"
        "} => {\n"
        "        :tarek :likes :cheese .\n"
        "\n"
        "    } .\n"
        "\n"
    )


def test_quoted_remove(store: LevelDB) -> None:

    qg = rdflib.graph.QuotedGraph(store=store, identifier=context2)
    qg.add((michel, likes, cheese))
    qg.remove((None, likes, cheese))
    qg.remove((None, None, cheese))

    assert len(qg) == 0


def test_conjunctivegraph_inclusion(store: LevelDB) -> None:

    cg1 = rdflib.ConjunctiveGraph(store=store)
    cg1.default_union = False
    cg1.bind("", EX)
    g11 = cg1.get_context(context1)
    g11.add((tarek, likes, pizza))
    g12 = cg1.get_context(context2)
    g12.add((tarek, hates, pizza))

    cg2 = rdflib.ConjunctiveGraph(store=store)
    cg2.default_union = False
    cg2.bind("", EX)
    g21 = cg2.get_context(context1)
    g21.add((tarek, likes, pizza))
    g22 = cg2.get_context(context2)
    g22.add((tarek, hates, pizza))

    assert cg2.serialize(format="trig") == (
        '@prefix : <urn:example:> .\n'
        '\n'
        ':context-1 {\n'
        '    :tarek :likes :pizza .\n'
        '}\n'
        '\n'
        ':context-2 {\n'
        '    :tarek :hates :pizza .\n'
        '}'
        '\n\n'
    )


def test_dataset_separation(store: LevelDB) -> None:

    ds1 = rdflib.Dataset(store=store)
    ds1.bind("", EX)
    g11 = ds1.graph(context1)
    g11.add((tarek, likes, pizza))
    g12 = ds1.graph(context2)
    g12.add((tarek, hates, pizza))

    assert ds1.serialize(format="trig") == (
        "@prefix : <urn:example:> .\n"
        "\n"
        ":context-1 {\n"
        "    :tarek :likes :pizza .\n"
        "}\n"
        "\n"
        ":context-2 {\n"
        "    :tarek :hates :pizza .\n"
        "}\n"
        "\n"
    )

    ds2 = rdflib.Dataset(store=store)
    ds2.bind("", EX)
    g21 = ds2.graph(context1)
    g21.add((tarek, hates, pizza))
    g22 = ds2.graph(context2)
    g22.add((tarek, likes, pizza))

    assert ds1.identifier != ds2.identifier

    # FAIL
    assert ds2.serialize(format="trig") == (
        "@prefix : <urn:example:> .\n"
        "\n"
        ":context-1 {\n"
        "    :tarek :hates :pizza ;\n"
        "        :likes :pizza .\n"
        "}\n"
        "\n:"
        "context-2 {\n"
        "    :tarek :hates :pizza ;\n"
        "        :likes :pizza .\n"
        "}\n"
        "\n"
    )

    # For eompleteness, note the duplicated context iris and the corresponding
    # duplicates in k2i/i2k
    #
    # print(store.dumpdb()
    # ...
    # indices
    #   cspo
    #     “2^3^4^5^” “”
    #     “2^3^8^5^” “”
    #     “7^3^4^5^” “”
    #     “7^3^8^5^” “”
    #     “^3^4^5^” “^2^7”
    #     “^3^8^5^” “^2^7”
    #   cpos
    #     “2^4^5^3^” “”
    #     “2^8^5^3^” “”
    #     “7^4^5^3^” “”
    #     “7^8^5^3^” “”
    #     “^4^5^3^” “^2^7”
    #     “^8^5^3^” “^2^7”
    #   cosp
    #     “2^5^3^4^” “”
    #     “2^5^3^8^” “”
    #     “7^5^3^4^” “”
    #     “7^5^3^8^” “”
    #     “^5^3^4^” “^2^7”
    #     “^5^3^8^” “^2^7”
    # lookup
    #   “0” = “['2^3^4^5^', '7^3^8^5^', '2^3^8^5^', '^3^8^5^', '7^3^4^5^', '^3^4^5^']”
    #   “1” = “['2^3^4^5^', '7^3^8^5^', '2^3^8^5^', '^3^8^5^', '7^3^4^5^', '^3^4^5^']”
    #   “2” = “['2^4^5^3^', '7^8^5^3^', '2^8^5^3^', '^8^5^3^', '7^4^5^3^', '^4^5^3^']”
    #   “3” = “['2^3^4^5^', '7^3^8^5^', '2^3^8^5^', '^3^8^5^', '7^3^4^5^', '^3^4^5^']”
    #   “4” = “['2^5^3^4^', '7^5^3^8^', '2^5^3^8^', '^5^3^8^', '7^5^3^4^', '^5^3^4^']”
    #   “5” = “['2^5^3^4^', '7^5^3^8^', '2^5^3^8^', '^5^3^8^', '7^5^3^4^', '^5^3^4^']”
    #   “6” = “['2^4^5^3^', '7^8^5^3^', '2^8^5^3^', '^8^5^3^', '7^4^5^3^', '^4^5^3^']”
    #   “7” = “['2^3^4^5^', '7^3^8^5^', '2^3^8^5^', '^3^8^5^', '7^3^4^5^', '^3^4^5^']”
    # namespaces:
    #   “brick” = “https://brickschema.org/schema/Brick#”
    #   ...
    #   “xml” = “http://www.w3.org/XML/1998/namespace”
    #   “” = “urn:example:”
    # contexts:
    #   ctxt 1 = b'' “urn:example:context-1”
    #   ctxt 6 = b'' “urn:example:context-2”
    #   ctxt 9 = b'' “urn:x-rdflib:default”
    #   ctxt 2 =  “<urn:example:context-1> a rdfg:Graph;rdflib:storage [a rdflib:Store;rdfs:label 'LevelDB'].”
    #   ctxt 7 =  “<urn:example:context-2> a rdfg:Graph;rdflib:storage [a rdflib:Store;rdfs:label 'LevelDB'].”
    # indexed identifiers (k2i/i2k):
    #   ctxt b'urn:example:context-1' = 1
    #   ctxt b'urn:example:context-1' = 2
    #   term b'urn:example:tarek' = 3
    #   term b'urn:example:likes' = 4
    #   term b'urn:example:pizza' = 5
    #   ctxt b'urn:example:context-2' = 6
    #   ctxt b'urn:example:context-2' = 7
    #   term b'urn:example:hates' = 8
    #   total: 12


def test_reopen_store(store: LevelDB) -> None:

    store.add((tarek, likes, pizza), context=None)  # type: ignore[arg-type]
    store.commit()
    store.close()

    rt = store.open(path, create=False)
    assert rt == rdflib.store.VALID_STORE, "The underlying store is corrupt"
    assert len(list(store.triples((None, None, None), context=None))) == 1


@pytest.mark.xfail(reason="Forbidden to overwrite existing database", raises=Exception)
def test_overwrte_existing(store: LevelDB) -> None:

    store.add((tarek, likes, pizza), context2, quoted=False)
    store.commit()
    store.close()

    store = LevelDB(identifier=context0)
    _ = store.open(path, create=True)
    store.close()
    store.destroy(path)


@pytest.mark.xfail(reason="cannot add None graph", raises=TypeError)
def test_add_graph_as_none(store: LevelDB) -> None:
    store.add_graph(None)  # type: ignore[arg-type]


@pytest.mark.xfail(reason="cannot remove None graph", raises=TypeError)
def test_remove_graph_as_none(store: LevelDB) -> None:
    store.remove_graph(None)  # type: ignore[arg-type]


def test___to_string(store: LevelDB) -> None:
    store.add((tarek, likes, pizza), context2, quoted=False)
    assert store._to_string(tarek) == '1'
    assert store._from_string('1') == tarek


@pytest.mark.xfail(reason="expected Exception Key for 100000 is None", raises=Exception)
def test_from_string_key_exception(store: LevelDB) -> None:
    _ = store._from_string("100000")


def test_literals(store: LevelDB) -> None:
    params = [
        ("'", rdflib.XSD.string),
        ('"', rdflib.XSD.string),
        ('\\' + chr(92), rdflib.XSD.string),
        ("true", rdflib.XSD.boolean),
        ("1", rdflib.XSD.boolean),
        (b"false", rdflib.XSD.boolean),
        (b"0", rdflib.XSD.boolean),
        (b"-128", rdflib.XSD.byte),
        ("127", rdflib.XSD.byte),
        ("255", rdflib.XSD.unsignedByte),
        (b"200", rdflib.XSD.unsignedByte),
        ("-6000", rdflib.XSD.short),
        ("1000000", rdflib.XSD.nonNegativeInteger),
        ("0", rdflib.XSD.double),
        ("0.1", rdflib.XSD.double),
        ("0.1", rdflib.XSD.decimal),
        ("2147483647", rdflib.XSD.int),
        ("2147483648", rdflib.XSD.integer),
        ("valid ASCII", rdflib.XSD.string),
        ("7264666c6962", rdflib.XSD.hexBinary),
    ]

    for lexical, datatype in params:
        lit = rdflib.Literal(lexical, datatype=datatype)
        # If the literal is not ill typed it should have a value associated with it.
        store.add((context0, rdflib.RDFS.label, lit), context=None)
        assert next(store.triples((context0, rdflib.RDFS.label, None), context=None))[0][2] == lit
        store.remove((context0, rdflib.RDFS.label, lit), context=None)


def test_persistence_of_unpopulated_contexts(store: LevelDB) -> None:

    assert len(list(store.contexts())) == 0

    store.add_graph(context2)

    assert len(list(store.contexts())) == 1

    store.add((tarek, likes, pizza), context=context2)
    assert len(list(store.triples((None, None, None), context=context2))) == 1
    store.remove((tarek, likes, pizza), context=context2)
    assert len(list(store.triples((None, None, None), context=context2))) == 0

    assert len(list(store.contexts())) == 1

    store.remove_graph(context2)

    assert len(list(store.contexts())) == 0


@pytest.mark.xfail(reason="WIP")
def test_sync(store: LevelDB) -> None:
    store.sync()
    store.close()
    store.sync()
