import pytest
import typing
import tempfile
import shutil
import os
import rdflib
from .data import TEST_DATA_DIR, michel, likes, cheese, pizza, storename

path = os.path.join(tempfile.gettempdir(), f"test_{storename.lower()}.db")


@pytest.fixture
def graph() -> typing.Generator[rdflib.Graph, None, None]:

    if os.path.exists(path):
        try:
            shutil.rmtree(path)
        except Exception:
            os.unlink(path)

    _g = rdflib.Graph(store=storename)
    rt = _g.open(path, create=True)
    assert rt == rdflib.store.VALID_STORE, "The underlying store is corrupt"
    assert (
        len(_g) == 0
    ), "There must be zero triples in the graph just after store (file) creation"
    data = """
            PREFIX : <https://example.org/>

            :a :b :c .
            :d :e :f .
            :d :g :h .
            """
    _g.parse(data=data, format="ttl")
    assert (
        len(_g) == 3
    ), "There must be three triples in the graph after the first data chunk parse"
    yield _g

    _g.store.close()
    _g.store.destroy(configuration=path)


def test_create_db(graph: rdflib.Graph) -> None:
    graph.add((michel, likes, pizza))
    graph.add((michel, likes, cheese))
    graph.commit()
    assert len(graph) == 5


def test_escape_quoting(graph: rdflib.Graph) -> None:
    assert (
        len(graph) == 3
    ), "There must be three triples in the graph after the first data chunk parse"
    test_string = "That’s a Literal!!"
    graph.add(
        (
            rdflib.URIRef("http://example.org/foo"),
            rdflib.RDFS.label,
            rdflib.Literal(test_string, datatype=rdflib.XSD.string),
        )
    )
    graph.commit()
    assert ("That’s a Literal!!") in graph.serialize(format="xml")


def test_missing_db_exception(graph: rdflib.Graph) -> None:
    graph.store.close()
    graph.store.destroy(path)

    graph.store.open(path, create=True)
    ntriples = graph.triples((None, None, None))
    assert len(list(ntriples)) == 0


def test_reopening_db(graph: rdflib.Graph) -> None:

    graph.add((michel, likes, pizza))
    graph.add((michel, likes, cheese))
    graph.commit()
    graph.store.close()
    graph.store.open(path, create=False)
    ntriples = graph.triples((None, None, None))
    listntriples = list(ntriples)
    assert len(listntriples) == 5, f"Expected 2 not {len(listntriples)}"


def test_reopening_missing_db_returns_no_store(graph: rdflib.Graph) -> None:
    graph.store.close()
    graph.store.destroy(path)
    assert graph.open(path, create=False) == rdflib.store.NO_STORE


def test_isopen_db(graph: rdflib.Graph) -> None:
    assert graph.store.is_open() is True  # type: ignore[attr-defined]
    graph.store.close()
    assert graph.store.is_open() is False  # type: ignore[attr-defined]


def test_empty_literal(graph: rdflib.Graph) -> None:
    graph.remove((None, None, None))
    graph.add(
        (
            rdflib.URIRef("http://example.com/s"),
            rdflib.URIRef("http://example.com/p"),
            rdflib.Literal(""),
        )
    )

    o = tuple(graph)[0][2]
    assert o == rdflib.Literal(""), repr(o)


def test_write_parsed(graph: rdflib.Graph) -> None:

    assert (
        len(graph) == 3
    ), "There must be three triples in the graph after the first data chunk parse"

    data2 = """
            PREFIX : <https://example.org/>

            :d :i :j .
            """
    graph.parse(data=data2, format="ttl")
    assert (
        len(graph) == 4
    ), "There must be four triples in the graph after the second data chunk parse"

    data3 = """
            PREFIX : <https://example.org/>

            :d :i :j .
            """
    graph.parse(data=data3, format="ttl")
    assert (
        len(graph) == 4
    ), "There must still be four triples in the graph after the thrd data chunk parse"


def test_sparql_query(graph: rdflib.Graph) -> None:

    q = """
        PREFIX : <https://example.org/>

        SELECT (COUNT(*) AS ?c)
        WHERE {
            :d ?p ?o .
        }"""

    c = 0
    for row in graph.query(q):
        c = int(row.c)  # type: ignore[union-attr]
    assert c == 2, "SPARQL COUNT must return 2"


def test_sparql_insert(graph: rdflib.Graph) -> None:

    q = """
        PREFIX : <https://example.org/>

        INSERT DATA {
            :x :y :z .
        }"""

    graph.update(q)
    assert len(graph) == 4, "After extra triple insert, length must be 4"


def test_reopen_store(graph: rdflib.Graph) -> None:

    assert len(graph) == 3, "Initially we must have 3 triples from setUp"
    graph.close()
    del graph

    # reopen the graph
    graph = rdflib.Graph("LevelDB")
    graph.open(path, create=False)
    assert (
        len(graph) == 3
    ), "After close and reopen, we should still have the 3 originally-added triples"
    graph.close()
    graph.destroy(path)


def test_timbl_card(graph: rdflib.Graph) -> None:
    data = open(TEST_DATA_DIR / "timbl-card.n3").read()
    # Parse in an RDF file hosted on the Internet
    # graph.parse("http://www.w3.org/People/Berners-Lee/card")
    graph.parse(data=data, format="n3")

    # Loop through each triple in the graph (subj, pred, obj)
    for subj, pred, obj in graph:
        # Check if there is at least one triple in the Graph
        if (subj, pred, obj) not in graph:
            raise Exception("It better be!")

    assert len(graph) == 89, f"Expected to read 89 triples, not {len(graph)}"
