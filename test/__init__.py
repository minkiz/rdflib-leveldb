from rdflib import plugin, store

plugin.register(
    "LevelDB",
    store.Store,
    "rdflib_leveldb.leveldb",
    "LevelDB"
)
