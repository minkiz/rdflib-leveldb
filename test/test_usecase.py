import pytest
import os
import shutil
import tempfile
import typing
import logging
import rdflib
from rdflib_leveldb.leveldb import LevelDB
from .data import context0, context1, storename

logging.basicConfig(level=logging.ERROR, format="%(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

path = os.path.join(tempfile.gettempdir(), f"test_{storename.lower()}.db")


@pytest.fixture(scope="function")
def store() -> typing.Generator[rdflib.store.Store, None, None]:
    if os.path.exists(path):
        shutil.rmtree(path)

    _s = LevelDB(path, identifier=context1)
    yield _s
    _s.close()
    _s.destroy(path)

    if os.path.exists(path):
        shutil.rmtree(path)


@pytest.fixture(scope="function")
def graph() -> typing.Generator[rdflib.Graph, None, None]:
    if os.path.exists(path):
        shutil.rmtree(path)

    # s = LevelDB(path, identifier=context1)
    _g = rdflib.Graph(store="LevelDB", identifier=context0)
    assert (
        len(_g) == 0
    ), "There must be zero triples in the graph just after store (file) creation"

    yield _g

    _g.store.close()
    _g.store.destroy(path)

    if os.path.exists(path):
        shutil.rmtree(path)
