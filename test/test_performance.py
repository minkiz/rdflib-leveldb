import pytest
import gc
import logging
import os
import shutil
import typing
import tempfile
from time import time
import rdflib
from rdflib_leveldb.leveldb import LevelDB
from test.data import context0, storename

logging.basicConfig(level=logging.ERROR, format="%(message)s")
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)

path = os.path.join(tempfile.gettempdir(), f"test_{storename.lower()}.db")


@pytest.fixture
def store() -> typing.Generator[LevelDB, None, None]:
    gcold = gc.isenabled()
    gc.collect()
    gc.disable()

    if os.path.exists(path):
        shutil.rmtree(path)

    _s = LevelDB(path, identifier=context0)
    yield _s

    _s.close()
    _s.destroy(configuration=path)
    if gcold:
        gc.enable()

    if os.path.exists(path):
        shutil.rmtree(path)


def test_add_triples(store: LevelDB) -> None:
    store.add((None, None, None), None)
