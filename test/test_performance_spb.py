import pytest
import gc
import os
import shutil
import typing
import logging
from time import time
import tempfile
from rdflib import Graph
from rdflib.store import VALID_STORE
from test.data import TEST_DATA_DIR, storename

logging.basicConfig(level=logging.ERROR, format="%(message)s")
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
path = os.path.join(tempfile.gettempdir(), f"test_{storename.lower()}.db")


fixturelist = [
    # ("500triples", 691),
    # ("1ktriples", 1285),
    # ("2ktriples", 2006),
    # ("3ktriples", 3095),
    # ("5ktriples", 5223),
    # ("10ktriples", 10303),
    ("25ktriples", 25161),
    # ("50ktriples", 50168),
    # ("100ktriples", 100073),
    # ("250ktriples", 250128),
    # ("500ktriples", 500043),
]


@pytest.fixture(scope="function", params=fixturelist)
def get_graph(request: pytest.FixtureRequest) -> typing.Generator[tuple[tuple[str, int], Graph], None, None]:
    gcold = gc.isenabled()
    gc.collect()
    gc.disable()

    if os.path.exists(path):
        try:
            shutil.rmtree(path)
        except Exception:
            pass

    graph = Graph(store=storename)
    rt = graph.open(path, create=True)
    assert rt == VALID_STORE, "The underlying store is corrupt"
    assert (
        len(graph) == 0
    ), "There must be zero triples in the graph just after store (file) creation"
    yield request.param, graph

    if gcold:
        gc.enable()

    graph.close()
    graph.destroy(configuration=path)

    del graph

    if os.path.exists(path):
        try:
            shutil.rmtree(path)
        except Exception:
            pass


def test_graph_parse_and_open(get_graph: tuple[tuple[str, int], Graph]) -> None:
    fixturedata, graph = get_graph

    inputloc = str(TEST_DATA_DIR / "sp2b" / f"{fixturedata[0]}.nt")
    ntriples = fixturedata[1]
    t0 = time()
    graph.parse(location=inputloc, format="nt")
    t1 = time()

    res = f"{t1 - t0:.3f}"
    assert len(graph) == ntriples, len(graph)
    log.debug(f"Parsed and loaded {len(graph):5d} triples in {res.strip()}s")
    graph.close()

    t0 = time()
    graph.open(path, create=False)
    t1 = time()

    res = f"{t1 - t0:.3f}"
    log.debug(f"Loaded {ntriples} triples in {res.strip()}s")

    t0 = time()
    for _i in graph.triples((None, None, None)):
        pass
    t1 = time()
    log.debug(f"Iterating: {t1 - t0:.3f}s")

    t0 = time()
    lg = len(graph)
    t1 = time()
    log.debug(f"Calculating len of {lg} {t1 - t0:.3f}s")


@pytest.mark.skip("WIP")
def test_load_from_graph(get_graph: tuple[tuple[str, int], Graph]) -> None:
    fixturedata, graph = get_graph

    inputloc = str(TEST_DATA_DIR / "sp2b" / f"{fixturedata[0]}.n3")

    store = graph.store
    input = Graph()
    t0 = time()
    input.parse(location=inputloc, format="n3")
    store.addN(tuple(t) + (graph,) for t in input)
    t1 = time()
    res = f"{t1 - t0:.3f}"
    log.debug(f"Loaded {len(graph):5d} triples in {res.strip()}s")
    graph.close()

    t0 = time()
    graph.open(path, create=False)
    t1 = time()

    res = f"{t1 - t0:.3f}"
    log.debug(f"Loaded {len(graph):5d} triples in {res.strip()}s")

    t0 = time()
    for _i in graph.triples((None, None, None)):
        pass
    t1 = time()
    log.debug(f"Iterating: {t1 - t0:.3f}s")

    assert len(graph) == fixturedata[1], len(graph)

