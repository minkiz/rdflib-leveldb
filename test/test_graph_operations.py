import pytest
import typing
import tempfile
import shutil
import os
import rdflib
from .data import tarek, bob, michel, hates, likes, cheese, pizza, storename, context0

path = os.path.join(tempfile.gettempdir(), f"test_{storename.lower()}.db")


@pytest.fixture
def graph() -> typing.Generator[rdflib.Graph, None, None]:

    if os.path.exists(path):
        try:
            shutil.rmtree(path)
        except Exception:
            os.unlink(path)

    _g = rdflib.Graph(store=storename)
    rt = _g.open(path, create=True)
    assert rt == rdflib.store.VALID_STORE, "The underlying store is corrupt"
    assert (
        len(_g) == 0
    ), "There must be zero triples in the graph just after store (file) creation"
    data = """
            PREFIX : <https://example.org/>

            :a :b :c .
            :d :e :f .
            :d :g :h .
            """
    _g.parse(data=data, format="ttl")

    assert (
        len(_g) == 3
    ), "There must be three triples in the graph after the first data chunk parse"

    yield _g

    _g.store.close()
    _g.store.destroy(configuration=path)


def test_write_parsed(graph: rdflib.Graph) -> None:

    assert (
        len(graph) == 3
    ), "There must be three triples in the graph after the first data chunk parse"

    data2 = """
            PREFIX : <https://example.org/>

            :d :i :j .
            """
    graph.parse(data=data2, format="ttl")

    assert (
        len(graph) == 4
    ), "There must be four triples in the graph after the second data chunk parse"

    data3 = """
            PREFIX : <https://example.org/>

            :d :i :j .
            """

    graph.parse(data=data3, format="ttl")
    assert (
        len(graph) == 4
    ), "There must still be four triples in the graph after the thrd data chunk parse"


def add_stuff(graph: rdflib.Graph) -> None:
    graph.remove((None, None, None))
    graph.add((tarek, likes, pizza))
    graph.add((tarek, likes, cheese))
    graph.add((michel, likes, pizza))
    graph.add((michel, likes, cheese))
    graph.add((bob, likes, cheese))
    graph.add((bob, hates, pizza))
    graph.add((bob, hates, michel))  # gasp!
    graph.commit()
    assert len(graph) == 7


def remove_stuff(graph: rdflib.Graph) -> None:
    graph.remove((tarek, likes, pizza))
    graph.remove((tarek, likes, cheese))
    graph.remove((michel, likes, pizza))
    graph.remove((michel, likes, cheese))
    graph.remove((bob, likes, cheese))
    graph.remove((bob, hates, pizza))
    graph.remove((bob, hates, michel))  # gasp!
    graph.commit()


def test_add_stuff(graph: rdflib.Graph) -> None:
    add_stuff(graph)
    assert len(graph) == 7


def test_remove_stuff(graph: rdflib.Graph) -> None:
    add_stuff(graph)
    assert len(graph) == 7
    remove_stuff(graph)
    assert len(graph) == 0, list(graph)


def test_graph_add_and_remove_triples(graph: rdflib.Graph) -> None:
    graph.remove((None, None, None))

    triples = graph.triples
    Any = None  # noqa: N806

    add_stuff(graph)

    # unbound subjects
    assert len(list(triples((Any, likes, pizza)))) == 2
    assert len(list(triples((Any, hates, pizza)))) == 1
    assert len(list(triples((Any, likes, cheese)))) == 3
    assert len(list(triples((Any, hates, cheese)))) == 0

    # unbound objects
    assert len(list(triples((michel, likes, Any)))) == 2
    assert len(list(triples((tarek, likes, Any)))) == 2
    assert len(list(triples((bob, hates, Any)))) == 2
    assert len(list(triples((bob, likes, Any)))) == 1

    # unbound predicates
    assert len(list(triples((michel, Any, cheese)))) == 1
    assert len(list(triples((tarek, Any, cheese)))) == 1
    assert len(list(triples((bob, Any, pizza)))) == 1
    assert len(list(triples((bob, Any, michel)))) == 1

    # unbound subject, objects
    assert len(list(triples((Any, hates, Any)))) == 2
    assert len(list(triples((Any, likes, Any)))) == 5

    # unbound predicates, objects
    assert len(list(triples((michel, Any, Any)))) == 2
    assert len(list(triples((bob, Any, Any)))) == 3
    assert len(list(triples((tarek, Any, Any)))) == 2

    # unbound subjects, predicates
    assert len(list(triples((Any, Any, pizza)))) == 3
    assert len(list(triples((Any, Any, cheese)))) == 3
    assert len(list(triples((Any, Any, michel)))) == 1

    # all unbound
    assert len(list(triples((Any, Any, Any)))) == 7
    remove_stuff(graph)
    assert len(list(triples((Any, Any, Any)))) == 0


def test_multiple_graphs_sharing_store(graph: rdflib.Graph) -> None:

    alice = rdflib.URIRef("alice")

    g1 = rdflib.Graph()
    g1.add((alice, rdflib.RDF.value, pizza))

    g2 = rdflib.Graph()
    g2.add((bob, rdflib.RDF.value, pizza))

    gv1 = rdflib.Graph(store=graph.store, base=g1.identifier)
    gv2 = rdflib.Graph(store=graph.store, base=g2.identifier)

    graph.add((gv1, rdflib.RDF.value, gv2))

    v = graph.value(gv1)
    assert gv2 == v


def test_graph_connected(graph: rdflib.Graph) -> None:

    add_stuff(graph)
    assert graph.connected() is True

    jeroen = rdflib.URIRef("jeroen")
    unconnected = rdflib.URIRef("unconnected")

    graph.add((jeroen, likes, unconnected))

    assert graph.connected() is False


def test_graph_sub(graph: rdflib.Graph) -> None:
    g1 = rdflib.Graph(graph.store)
    g2 = rdflib.Graph(graph.store)

    g1.add((tarek, likes, pizza))
    g1.add((bob, likes, cheese))

    g2.add((bob, likes, cheese))

    g3 = g1 - g2

    assert len(g3) == 1
    assert (tarek, likes, pizza) in g3
    assert (tarek, likes, cheese) not in g3

    assert (bob, likes, cheese) not in g3

    g1 -= g2

    assert len(g1) == 1
    assert (tarek, likes, pizza) in g1
    assert (tarek, likes, cheese) not in g1

    assert (bob, likes, cheese) not in g1


def test_graph_add(graph: rdflib.Graph) -> None:
    g1 = rdflib.Graph(graph.store)
    g2 = rdflib.Graph(graph.store)

    g1.add((tarek, likes, pizza))

    g2.add((bob, likes, cheese))

    g3 = g1 + g2

    assert len(g3) == 2
    assert (tarek, likes, pizza) in g3
    assert (tarek, likes, cheese) not in g3

    assert (bob, likes, cheese) in g3

    g1 += g2

    assert len(g1) == 2
    assert (tarek, likes, pizza) in g1
    assert (tarek, likes, cheese) not in g1

    assert (bob, likes, cheese) in g1


def test_graph_intersection(graph: rdflib.Graph) -> None:
    g1 = rdflib.Graph(graph.store)
    g2 = rdflib.Graph(graph.store)

    g1.add((tarek, likes, pizza))
    g1.add((michel, likes, cheese))

    g2.add((bob, likes, cheese))
    g2.add((michel, likes, cheese))

    g3 = g1 * g2

    assert len(g3) == 1
    assert (tarek, likes, pizza) not in g3
    assert (tarek, likes, cheese) not in g3

    assert (bob, likes, cheese) not in g3

    assert (michel, likes, cheese) in g3

    g1 *= g2

    assert len(g1) == 1

    assert (tarek, likes, pizza) not in g1
    assert (tarek, likes, cheese) not in g1

    assert (bob, likes, cheese) not in g1

    assert (michel, likes, cheese) in g1


xmltestdoc = """<?xml version="1.0" encoding="UTF-8"?>
<rdf:RDF
   xmlns="http://example.org/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
>
  <rdf:Description rdf:about="http://example.org/a">
    <b rdf:resource="http://example.org/c"/>
  </rdf:Description>
</rdf:RDF>
"""

n3testdoc = """@prefix : <http://example.org/> .

:a :b :c .
"""

nttestdoc = (
    "<http://example.org/a> <http://example.org/b> <http://example.org/c> .\n"
)
