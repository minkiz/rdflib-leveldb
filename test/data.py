from pathlib import Path
from rdflib import URIRef, Namespace

TEST_DIR = Path(__file__).parent
TEST_DATA_DIR = TEST_DIR / "data"

alice_uri = URIRef("http://example.org/alice")
bob_uri = URIRef("http://example.org/bob")

michel = URIRef("urn:example:michel")
tarek = URIRef("urn:example:tarek")
bob = URIRef("urn:example:bob")
likes = URIRef("urn:example:likes")
pizza = URIRef("urn:example:pizza")
hates = URIRef("urn:example:hates")
cheese = URIRef("urn:example:cheese")
context0 = URIRef("urn:example:context-0")
context1 = URIRef("urn:example:context-1")
context2 = URIRef("urn:example:context-2")
EX = Namespace("urn:example:")
EG = Namespace("urn:eggsample:")
EGDC = Namespace("http://example.com/")
storename = "LevelDB"

SIMPLE_TRIPLE_GRAPH = TEST_DATA_DIR / "simple_triple.py"

__all__ = [
    "TEST_DIR",
    "TEST_DATA_DIR",
    "SIMPLE_TRIPLE_GRAPH",
    "alice_uri",
    "bob_uri",
    "michel",
    "tarek",
    "bob",
    "likes",
    "hates",
    "pizza",
    "cheese",
    "context0",
    "context1",
    "context2",
    "EX",
]
